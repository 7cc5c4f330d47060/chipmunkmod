# ChipmunkMod
My fork of [Blackilykat's fork of ChipmunkMod](https://code.chipmunk.land/Blackilykat/chipmunkmod). Many of the Blackilykat features have been removed.

# Installation

Download the JAR file (not the source code archive unless you want to build it) of the latest release from the Releases tab, make sure you have the Fabric loader and Fabric API installed for version 1.21.1, then copy the JAR file to your mods folder. Alternatively, you may follow the instructions below to get the latest code.

# Building

Download the code by clicking the 3 dots icon on the right of the screen below the repository info then `Download ZIP` or `Download TAR.GZ`, then extract it.

Alternatively, you can download using the Git client:
`git clone https://code.chipmunk.land/7cc5c4f330d47060/chipmunkmod.git`

Make sure you have a Java 21 JDK installed (this may not work for newer JDK versions), then go to the ChipmunkMod directory and run `./gradlew build` for Unix(-like) OSes or `gradlew.bat build` for Windows. If the build was successful, the compiled JAR file should be in `build/libs`.

Make sure you have the Fabric loader and Fabric API installed for version 1.21.4 (the main branch is on a newer game version than the release), and copy the JAR file to your mods folder.

If that's not clear enough, search how to install a Fabric mod on a search engine.

If errors appear that you don't understand, [create an issue](https://code.chipmunk.land/7cc5c4f330d47060/chipmunkmod/issues/new) about it.

 # Extra information
 
 The mod icon used (https://commons.wikimedia.org/wiki/File:Streifenhoernchen.jpg) is in the public domain.
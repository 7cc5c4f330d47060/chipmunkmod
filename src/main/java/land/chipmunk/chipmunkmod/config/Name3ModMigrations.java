package land.chipmunk.chipmunkmod.config;

import land.chipmunk.chipmunkmod.config.migration.AbstractMigrationManager;
import land.chipmunk.chipmunkmod.config.migrations.MigrationName3V0;

public final class Name3ModMigrations extends AbstractMigrationManager {
    public Name3ModMigrations() {
        super("version_name3");

        this.register(new MigrationName3V0()); // unversioned -> v0
    }
}

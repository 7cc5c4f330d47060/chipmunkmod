package land.chipmunk.chipmunkmod.config.migrations;

import land.chipmunk.chipmunkmod.config.migration.ConfigMigration;
import org.spongepowered.configurate.transformation.ConfigurationTransformation;
import org.spongepowered.configurate.transformation.TransformAction;

import static org.spongepowered.configurate.NodePath.path;

public final class MigrationName3V0 implements ConfigMigration {
    @Override
    public int version() {
        return 0;
    }

    @Override
    public ConfigurationTransformation create() {
        return ConfigurationTransformation.builder()
                .addAction(path("testbotWebhook"), TransformAction.remove())
                .build();
    }
}

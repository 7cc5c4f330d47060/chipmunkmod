package land.chipmunk.chipmunkmod.config;

import land.chipmunk.chipmunkmod.config.migration.AbstractMigrationManager;
import land.chipmunk.chipmunkmod.config.migrations.MigrationV0;
import land.chipmunk.chipmunkmod.config.migrations.MigrationV1;

public final class ChipmunkModMigrations extends AbstractMigrationManager {
    public ChipmunkModMigrations() {
        super("version");

        this.register(new MigrationV0()); // unversioned -> v0
        this.register(new MigrationV1());
    }
}

package land.chipmunk.chipmunkmod.mixin;

import com.google.gson.JsonObject;
import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.data.ChomeNSBotCommand;
import land.chipmunk.chipmunkmod.modules.ChomeNSBotCommandSuggestions;
import land.chipmunk.chipmunkmod.util.BotValidationUtilities;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.ChatInputSuggestor;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Mixin(value = net.minecraft.client.gui.screen.ChatScreen.class)
public abstract class ChatScreenMixin extends Screen {
    @Shadow
    private String originalChatText;

    @Shadow
    private int messageHistoryIndex;

    @Shadow
    protected TextFieldWidget chatField;

    @Shadow
    ChatInputSuggestor chatInputSuggestor;

    @Shadow
    protected abstract void onChatFieldUpdate(String chatText);

    protected ChatScreenMixin(Text title) {
        super(title);
    }

    // infinite chat
    @Inject(method = "init", at = @At("HEAD"), cancellable = true)
    protected void init(CallbackInfo ci) {
        final MinecraftClient client = MinecraftClient.getInstance();

        this.messageHistoryIndex = client.inGameHud.getChatHud().getMessageHistory().size();
        this.chatField = new TextFieldWidget(client.advanceValidatingTextRenderer, 4, this.height - 12, this.width - 4, 12, Text.translatable("chat.editBox")) {
            protected MutableText getNarrationMessage() {
                return super.getNarrationMessage().append(ChatScreenMixin.this.chatInputSuggestor.getNarration());
            }
        };
        this.chatField.setMaxLength(Integer.MAX_VALUE);
        this.chatField.setDrawsBackground(false);
        this.chatField.setText(this.originalChatText);
        this.chatField.setChangedListener(this::onChatFieldUpdate);
        this.chatField.setFocusUnlocked(false);
        this.addSelectableChild(this.chatField);
        this.chatInputSuggestor = new ChatInputSuggestor(this.client, this, this.chatField, this.textRenderer, false, false, 1, 10, true, -805306368);
        this.chatInputSuggestor.setCanLeave(false);
        this.chatInputSuggestor.refresh();

        ci.cancel();
    }

    @Inject(method = "sendMessage", at = @At("HEAD"), cancellable = true)
    private void sendMessage(String chatText, boolean addToHistory, CallbackInfo cir) {
        final MinecraftClient client = MinecraftClient.getInstance();

        if (addToHistory) {
            client.inGameHud.getChatHud().addToMessageHistory(chatText);
        }

        if (ChipmunkMod.CONFIG.bots.testbot.webhookUrl != null && chatText.startsWith(ChipmunkMod.CONFIG.bots.testbot.prefix)) {
            ChipmunkMod.executorService.submit(() -> {
                try {
                    final URL url = new URI(ChipmunkMod.CONFIG.bots.testbot.webhookUrl).toURL();

                    final HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                    connection.addRequestProperty("Content-Type", "application/json");
                    connection.addRequestProperty("User-Agent", "ChipmunkMod");
                    connection.setDoOutput(true);
                    connection.setRequestMethod("POST");

                    final JsonObject jsonObject = new JsonObject();

                    jsonObject.addProperty("username", "ChipmunkMod UwU");
                    jsonObject.addProperty("content", MinecraftClient.getInstance().getSession().getUsername());

                    final OutputStream stream = connection.getOutputStream();
                    stream.write(jsonObject.toString().getBytes());
                    stream.flush();
                    stream.close();

                    connection.getInputStream().close();
                    connection.disconnect();
                } catch (IOException | URISyntaxException e) {
                    e.printStackTrace();
                }
            });
        } else if (chatText.startsWith(ChipmunkMod.CONFIG.bots.chomens.prefix)) {
            final List<ChomeNSBotCommand> commands = ChomeNSBotCommandSuggestions.INSTANCE.commands;

            final List<String> moreOrTrustedCommands = commands.stream()
                    .filter(command -> command.trustLevel() != ChomeNSBotCommand.TrustLevel.PUBLIC)
                    .map(command -> command.name().toLowerCase())
                    .toList();

            final List<String> aliases = new ArrayList<>();
            for (ChomeNSBotCommand command : commands) {
                if (command.trustLevel() == ChomeNSBotCommand.TrustLevel.PUBLIC) continue;

                aliases.addAll(command.aliases());
            }

            final String chatCommand = chatText.toLowerCase().split("\\s")[0];

            final int prefixLength = ChipmunkMod.CONFIG.bots.chomens.prefix.length();

            if (
                    moreOrTrustedCommands.contains(chatCommand) ||
                            aliases.contains(chatCommand.substring(prefixLength))
            ) {
                try {
                    BotValidationUtilities.chomens(chatText.substring(prefixLength));

                    cir.cancel();

                    return;
                } catch (Exception ignored) {
                }
            }
        }

        if (client.player == null) return;

        if (chatText.startsWith("/")) {
            client.player.networkHandler.sendChatCommand(chatText.substring(1));
        } else {
            client.player.networkHandler.sendChatMessage(chatText);
        }

        cir.cancel();
    }
}

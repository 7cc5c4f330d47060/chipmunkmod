package land.chipmunk.chipmunkmod.listeners;

import net.minecraft.network.packet.Packet;
import net.minecraft.text.Text;

public interface Listener {
    default void packetReceived (Packet<?> packet) {}
    default void chatMessageReceived (Text message) {}
    default void packetSent (Packet<?> packet) {}

    default void coreReady () {}
    default void coreMoved () {}
}

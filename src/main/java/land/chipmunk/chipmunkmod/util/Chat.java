package land.chipmunk.chipmunkmod.util;

import land.chipmunk.chipmunkmod.ChipmunkMod;
import net.minecraft.client.MinecraftClient;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

import java.time.Instant;
import java.util.Random;

public class Chat {
	public static final MinecraftClient MC = MinecraftClient.getInstance();
	
	public static void send(String message) {
		MC.inGameHud.getChatHud().addMessage(Text.of(message));
	}
	public static void send(Text message) {
		MC.inGameHud.getChatHud().addMessage(message);
	}
	public static void sendRed(String message) {
		send(Text.literal(message).formatted(Formatting.RED));
	}
	public static void sendGreen(String message) {
		send(Text.literal(message).formatted(Formatting.GREEN));
	}
	public static void sendGold(String message) {
		send(Text.literal(message).formatted(Formatting.GOLD));
	}
	
}

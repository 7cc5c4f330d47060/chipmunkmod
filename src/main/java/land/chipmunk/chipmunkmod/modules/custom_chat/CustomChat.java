package land.chipmunk.chipmunkmod.modules.custom_chat;

import com.google.common.hash.Hashing;
import land.chipmunk.chipmunkmod.ChipmunkMod;

import land.chipmunk.chipmunkmod.modules.Chat;
import land.chipmunk.chipmunkmod.modules.CommandCore;
import land.chipmunk.chipmunkmod.modules.KaboomCheck;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextReplacementConfig;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.network.ClientPlayerEntity;

import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

public class CustomChat {
    private static final LegacyComponentSerializer LEGACY = LegacyComponentSerializer.legacyAmpersand();
    private static final GsonComponentSerializer GSON = GsonComponentSerializer.gson();
    private static final CustomChatComponentRenderer RENDERER = new CustomChatComponentRenderer();

    // https://github.com/kaboomserver/extras/blob/master/src/main/java/pw/kaboom/extras/modules/player/PlayerChat.java#L49C9-L81C26
    private static final TextReplacementConfig URL_REPLACEMENT_CONFIG =
            TextReplacementConfig
                    .builder()
                    .match(Pattern
                            .compile("((https?://(ww(w|\\d)\\.)?|ww(w|\\d))[-a-zA-Z0-9@:%._+~#=]{1,256}"
                                    + "\\.[a-zA-Z0-9]{2,6}\\b([-a-zA-Z0-9@:%_+.~#?&/=]*))"))
                    .replacement((b, c) -> {
                        if (c == null) {
                            return null;
                        }

                        if (b.groupCount() < 1) {
                            return null;
                        }

                        final String content = b.group(1);
                        final String url;

                    /*
                        Minecraft doesn't accept "www.google.com" as a URL
                        in click events
                     */
                        if (content.contains("://")) {
                            url = content;
                        } else {
                            url = "https://" + content;
                        }

                        return Component.text(content, NamedTextColor.BLUE)
                                .decorate(TextDecoration.UNDERLINED)
                                .clickEvent(ClickEvent.openUrl(url));
                    })
                    .build();

    private final MinecraftClient client;

    public static final CustomChat INSTANCE = new CustomChat(MinecraftClient.getInstance());

    public boolean enabled = true;

    public String format;

    private Timer timer;

    private int total = 0;

    public CustomChat (MinecraftClient client) {
        this.client = client;
    }

    public void init () {
        final TimerTask task = new TimerTask() {
            public void run () {
                tick();
            }
        };

        resetTotal();

        timer = new Timer();
        timer.schedule(task, 0, 50);
    }

    private void tick () {
        final ClientPlayNetworkHandler networkHandler = client.getNetworkHandler();

        if (networkHandler != null) return;

        resetTotal();
        cleanup();
    }

    public void resetTotal() {
        total = 0;
    }

    private void cleanup () {
        if (timer == null) return;

        timer.cancel();
        timer.purge();
    }

    public void chat (String message) {
        final ClientPlayerEntity player = client.player;
        if (player == null) return;
        if (!enabled || !player.hasPermissionLevel(2) || !player.isCreative()) {
            Chat.sendChatMessage(message, true);
            return;
        }

        final Component styledMessage = LEGACY.deserialize(message)
                .replaceText(URL_REPLACEMENT_CONFIG);

        final String username = MinecraftClient.getInstance().getSession().getUsername();
        final String key = ChipmunkMod.CONFIG.bots.chomens.formatKey;
        final String hash = key != null ?
                Hashing.sha256()
                    .hashString(key + total, StandardCharsets.UTF_8)
                    .toString()
                    .substring(0, 8) :
                "";

        total++;

        final CustomChatContext context = new CustomChatContext(player.getUuidAsString(), styledMessage,
                Map.of("MESSAGE", message, "USERNAME", username, "HASH", hash));
        final Component renderedFormat = RENDERER.render(ChipmunkMod.CONFIG.customChat.format, context)
                .compact();
        final String json = GSON.serialize(renderedFormat);

        CommandCore.INSTANCE.run((KaboomCheck.INSTANCE.isKaboom ? "minecraft:tellraw @a " : "tellraw @a ") + json);
    }
}

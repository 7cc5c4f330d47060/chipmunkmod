package land.chipmunk.chipmunkmod.modules.custom_chat;

import net.kyori.adventure.text.Component;

import java.util.Map;

public record CustomChatContext(String uuid, Component message, Map<String, String> args) {
}

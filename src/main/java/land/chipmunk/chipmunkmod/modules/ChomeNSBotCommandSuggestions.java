package land.chipmunk.chipmunkmod.modules;

import land.chipmunk.chipmunkmod.data.ChomeNSBotCommand;
import land.chipmunk.chipmunkmod.listeners.Listener;
import land.chipmunk.chipmunkmod.listeners.ListenerManager;
import land.chipmunk.chipmunkmod.util.UUIDUtilities;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.text.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ChomeNSBotCommandSuggestions implements Listener {
    public static final String ID = "chomens_bot_request_command_suggestion";

    public static ChomeNSBotCommandSuggestions INSTANCE = new ChomeNSBotCommandSuggestions(MinecraftClient.getInstance());

    private final MinecraftClient client;

    public boolean receivedSuggestions = false; // can be set through eval

    public List<ChomeNSBotCommand> commands = new ArrayList<>();

    public ChomeNSBotCommandSuggestions (MinecraftClient client) {
        this.client = client;

        ListenerManager.addListener(this);
    }

    public void init () {}

    @Override
    public void coreMoved () {
        if (!receivedSuggestions) forceRequest();
    }

    public void forceRequest () {
        final ClientPlayerEntity player = client.player;

        if (player == null) return;

        final String selector = UUIDUtilities.selector(player.getUuid());

        final Component component = Component
                .text(ID)
                .append(Component.text(selector));

        final String serialized = GsonComponentSerializer.gson().serialize(component);

        CommandCore.INSTANCE.run((KaboomCheck.INSTANCE.isKaboom ? "minecraft:tellraw " : "tellraw ") + "@a[team=chomens_bot] " + serialized);
    }

    @Override
    public void chatMessageReceived(Text message) {
        final List<Text> children = message.getSiblings();
        if (children.isEmpty()) return;

        final Text textComponent = children.getFirst();
        if (!textComponent.getString().equals(ID)) return;

        commands = children.stream()
            .skip(1)
            .map(ChomeNSBotCommand::fromText)
            .filter(Objects::nonNull)
            .toList();

        receivedSuggestions = true;
    }
}

package land.chipmunk.chipmunkmod.modules;

import com.mojang.brigadier.CommandDispatcher;
import net.minecraft.client.MinecraftClient;
import net.minecraft.command.CommandSource;

public class KaboomCheck {
    private static final String CHECKED_COMMAND_OP = "extras:prefix"; // Added circa 2018
    private static final String CHECKED_COMMAND = "minecraft:op"; // It'd be a bit weird for non-Kaboom servers to allow /op without OP...
    public static final KaboomCheck INSTANCE = new KaboomCheck();

    private final MinecraftClient client = MinecraftClient.getInstance();
    public boolean isKaboom = false;

    public void onJoin() {
        this.isKaboom = false;
    }

    public void onCommandTree(final CommandDispatcher<CommandSource> dispatcher) {
        assert client.player != null; // We can only receive this packet while in a server
        final String checkedCommand = client.player.getPermissionLevel() == 4
                ? CHECKED_COMMAND_OP : CHECKED_COMMAND;

        this.isKaboom = dispatcher.getRoot().getChild(checkedCommand) != null;
    }
}
